﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float moveSpeed;
    public float walkSpeed = 3.5f;
    public float sprintSpeed = 8f;
    public float jumpSpeed = 5f;
    public const int MAX_JUMP = 3;
    private int currentJump = 0;
    private float rayCastDistance = 0.15f;
    public LayerMask layerMask;

    private new Rigidbody rigidbody;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {

        float vert = Input.GetAxis("Vertical");
        float hor = Input.GetAxis("Horizontal");

        Vector3 moveDir = vert * transform.forward + hor * transform.right;

        rigidbody.MovePosition(transform.position + moveDir.normalized * Time.deltaTime * moveSpeed);

        //Dit is sprinten

        if (Input.GetKey("q") && IsGrounded())
        {
            moveSpeed = sprintSpeed;
            //moveSpeed = sprintSpeed;
            Debug.Log("beginsprint");
        }
        else
        {
            moveSpeed = walkSpeed;
            //Debug.Log("eindesprint");

        }

        //Dit is jumpen
        
        
            if (Input.GetKeyDown("space") && (MAX_JUMP > currentJump))
            {
                    rigidbody.velocity = new Vector3(rigidbody.velocity.x, 5, rigidbody.velocity.z);
                currentJump++;
            }
        
        if (IsGrounded() && currentJump > 0)
        {
            currentJump = 0;
        }
        
        //Dit is Stim

        if (Input.GetKeyDown("e"))
        {
            moveSpeed = moveSpeed *= 2;
            jumpSpeed = jumpSpeed *= 2;
        }
        else { jumpSpeed = jumpSpeed * 1; }

 
    }

    private bool IsGrounded() //checkt of de speler op de grond is
    {
        Debug.Log("raken");
        return Physics.Raycast(transform.position + new Vector3(0,0.1f,0), -Vector3.up, rayCastDistance, layerMask);
        
    }

}